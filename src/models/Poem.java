package models;

import org.bson.types.ObjectId;
import utils.PersianUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Poem {
    private int id;
    private int catID;
    private ObjectId catObjID;
    private String title;
    private String titleFull;
    private ObjectId objectId;
    private ObjectId poetID;
    private List<Verse> verses;

    public Poem(ResultSet resultSet, Connection connection) throws SQLException {
        this.id = resultSet.getInt(1);
        this.catID = resultSet.getInt(2);
        String title = PersianUtils.conversion(resultSet.getString(3));
        if (title.length() < 250) {
            this.title = title;
        } else {
            this.title = PersianUtils.trimNames(title);
            this.titleFull = title;
        }
        this.objectId =new ObjectId(resultSet.getString(4));
        this.poetID = getPoetID(connection);
        this.catObjID = getCatObjID(connection);
        List<Verse> verses = new ArrayList<>();
        ResultSet verseRes = connection.createStatement().executeQuery("SELECT * from verse WHERE poem_id="+ id);
        while (verseRes.next()) {
            Verse verse = new Verse(verseRes);
            if (verse.isValid) {
                verses.add(verse);
            }
        }
        this.verses = verses;

    }

    private ObjectId getPoetID (Connection connection) throws SQLException {
        ResultSet resultSet = connection.createStatement().executeQuery("SELECT * FROM `cat` WHERE `cat`.`id`=" + this.catID);
        if (resultSet.next()) {
            int id = resultSet.getInt(2);
            ResultSet resultSet1 = connection.createStatement().executeQuery("SELECT * FROM `poet` WHERE id=" + id);
            if (resultSet1.next()) {
                String objID = resultSet1.getString(5);
                return new ObjectId(objID);
            }
            throw new Error("Could not find poet for poem with the id of " + this.id);
        }
        throw new Error("Could not find poet for poem with the id of " + this.id);
    }

    private ObjectId getCatObjID(Connection connection) throws SQLException {
        ResultSet resultSet = connection.createStatement().executeQuery("SELECT * FROM `cat` WHERE `cat`.`id`=" + this.catID);
        if (resultSet.next()) {
            String objID = resultSet.getString(5);
            return new ObjectId(objID);
        }
        throw new Error("Could not find poet for poem with the id of " + this.id);
    }


    public int getId() {
        return id;
    }

    public int getCatID() {
        return catID;
    }

    public String getTitle() {
        return title;
    }

    public ObjectId getObjectId() {
        return objectId;
    }

    public ObjectId getPoetID() {
        return poetID;
    }

    public List<Verse> getVerses() {
        return verses;
    }

    public ObjectId getCatObjID() {
        return catObjID;
    }

    public String getTitleFull() {
        return titleFull;
    }

    public class Verse {
        private int id;
        private int poemID;
        private int vOrder;
        private boolean isFirst;
        private String text;
        private boolean isValid = true;
        private ObjectId objectId;

        Verse(ResultSet resultSet) throws SQLException {
            this.id = resultSet.getInt(1);
            this.poemID = resultSet.getInt(2);
            this.vOrder = resultSet.getInt(3);
            this.isFirst = resultSet.getInt(4) == 0;
            String temp = PersianUtils.conversion(resultSet.getString(5));
            if (temp.length() < 3) {
                System.out.println("***** INVALID verse : " + temp);
                isValid = false;
            }
            this.text = temp;
            this.objectId = new ObjectId();
        }

        public int getId() {
            return id;
        }

        public int getPoemID() {
            return poemID;
        }

        public int getvOrder() {
            return vOrder;
        }

        public boolean isFirst() {
            return isFirst;
        }

        public String getText() {
            return text;
        }

        public boolean isValid() {
            return isValid;
        }

        public ObjectId getObjectId() {
            return objectId;
        }
    }
}
