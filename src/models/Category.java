package models;

import org.bson.types.ObjectId;
import utils.PersianUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Category {
    private int id;
    private int poetID;
    private ObjectId poetObjID;
    private String title;
    private int parentID;
    private ObjectId objectId;
    private List<SubCat> subCats;
    private List<SubPoem> subPoems;

    public Category(ResultSet resultSet, Connection connection) throws SQLException {
        this.id = resultSet.getInt(1);
        this.poetID = resultSet.getInt(2);
        this.title = PersianUtils.conversion(resultSet.getString(3));
        this.parentID = resultSet.getInt(4);
        this.objectId = new ObjectId(resultSet.getString(5));
        List<SubCat> subCats = new ArrayList<>();
        ResultSet subCatRes = connection.createStatement().executeQuery("SELECT * from cat WHERE parent_id=" + id);
        while (subCatRes.next()) {
            subCats.add(new SubCat(subCatRes));
        }
        this.subCats = subCats;
        List<SubPoem> subPoems = new ArrayList<>();
        ResultSet subPoemRes = connection.createStatement().executeQuery("SELECT * from poem WHERE cat_id=" + id);
        while (subPoemRes.next()) {
            subPoems.add(new SubPoem(subPoemRes));
        }
        this.subPoems = subPoems;

        this.poetObjID = getPoetID(connection);
    }

    private ObjectId getPoetID(Connection connection) throws SQLException {
        ResultSet resultSet1 = connection.createStatement().executeQuery("SELECT * FROM `poet` WHERE id=" + poetID);
        if (resultSet1.next()) {
            String objID = resultSet1.getString(5);
            return new ObjectId(objID);
        }
        throw new Error("Could not find poet for poem with the id of " + this.id);
    }

    public int getId() {
        return id;
    }

    public int getPoetID() {
        return poetID;
    }

    public String getTitle() {
        return title;
    }

    public int getParentID() {
        return parentID;
    }

    public ObjectId getObjectId() {
        return objectId;
    }

    public List<SubCat> getSubCats() {
        return subCats;
    }

    public ObjectId getPoetObjID() {
        return poetObjID;
    }

    public List<SubPoem> getSubPoems() {
        return subPoems;
    }

    public class SubCat {
        private int id;
        private ObjectId objectId;

        SubCat(ResultSet resultSet) throws SQLException {
            this.id = resultSet.getInt(1);
            this.objectId = new ObjectId(resultSet.getString(5));
        }

        public int getId() {
            return id;
        }

        public ObjectId getObjectId() {
            return objectId;
        }
    }

    public class SubPoem {
        private int id;
        private ObjectId objectId;

        SubPoem(ResultSet resultSet) throws SQLException {
            this.id = resultSet.getInt(1);
            this.objectId = new ObjectId(resultSet.getString(4));
        }

        public int getId() {
            return id;
        }

        public ObjectId getObjectId() {
            return objectId;
        }

    }
}
