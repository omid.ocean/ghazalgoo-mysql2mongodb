package models;

import org.bson.types.ObjectId;
import utils.PersianUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Poet {
    private int id;
    private String name;
    private int catID;
    private String desc;
    private ObjectId objectId;
    private ObjectId catObjID;


    public Poet(ResultSet resultSet, Connection connection) throws SQLException {
        this.id = resultSet.getInt(1);
        this.name = PersianUtils.conversion(resultSet.getString(2));
        this.catID = resultSet.getInt(3);
        this.desc = PersianUtils.conversion(resultSet.getString(4));
        this.objectId = new ObjectId(resultSet.getString(5));
        this.catObjID = getCatObjID(connection);
    }

    private ObjectId getCatObjID(Connection connection) throws SQLException {
        ResultSet resultSet = connection.createStatement().executeQuery("SELECT * FROM `cat` WHERE `cat`.`id`=" + this.catID);
        if (resultSet.next()) {
            String objID = resultSet.getString(5);
            return new ObjectId(objID);
        }
        throw new Error("Could not find poet for poem with the id of " + this.id);
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getCatID() {
        return catID;
    }

    public String getDesc() {
        return desc;
    }

    public ObjectId getObjectId() {
        return objectId;
    }

    public ObjectId getCatObjID() {
        return catObjID;
    }

    @Override
    public String toString() {
        return "Poet{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", catID=" + catID +
                ", desc='" + desc + '\'' +
                ", objectId=" + objectId +
                '}';
    }
}
