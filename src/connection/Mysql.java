package connection;

import models.Category;
import models.Poem;
import models.Poet;
import org.bson.codecs.ObjectIdGenerator;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Mysql {
    private Connection con;
    public Mysql (boolean updateObjID) {
        System.out.println("\n\n***** MySQL JDBC Connection *****");
        try{
            Class.forName("com.mysql.jdbc.Driver");
            con= DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/ghazalgoo","root","");

            if (updateObjID) {
                addPoetObjectId();
                addCatObjectId();
                addPoemObjectId();
                System.out.println("\n\n***** UPDATING DONE *****");
            }
        }catch(Exception e){ e.printStackTrace();}
    }

    private synchronized  void addPoetObjectId() throws SQLException {
        Statement statement = con.createStatement();
        ResultSet rs = statement.executeQuery("SELECT id from poet");
        while (rs.next()) {
            System.out.println("poet with id :" + rs.getInt(1) + " UPDATED WITH NEW OBJECT_ID!");
            con.createStatement().executeUpdate("UPDATE poet SET objid='"+new ObjectIdGenerator().generate().toString()+"' WHERE id=" + rs.getInt(1));
        }
    }

    private synchronized  void addCatObjectId() throws SQLException {
        Statement statement = con.createStatement();
        ResultSet rs = statement.executeQuery("SELECT id from cat");
        while (rs.next()) {
            System.out.println("category with id :" + rs.getInt(1) + " UPDATED WITH NEW OBJECT_ID!");
            con.createStatement().executeUpdate("UPDATE cat SET objid='"+new ObjectIdGenerator().generate().toString()+"' WHERE id=" + rs.getInt(1));
        }
    }

    private synchronized  void addPoemObjectId() throws SQLException {
        Statement statement = con.createStatement();
        ResultSet rs = statement.executeQuery("SELECT id from poem");
        while (rs.next()) {
            System.out.println("poem with id :" + rs.getInt(1) + " UPDATED WITH NEW OBJECT_ID!");
            con.createStatement().executeUpdate("UPDATE poem SET objid='"+new ObjectIdGenerator().generate().toString()+"' WHERE id=" + rs.getInt(1));
        }
    }

    public synchronized List<Poet> getAllPoets() throws SQLException {
        List<Poet> poets = new ArrayList<>();
        Statement statement = con.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * from poet");
        while (rs.next()) {
            System.out.println("poet with id :" + rs.getInt(1) + " SELECTED!");
            poets.add(new Poet(rs, con));
        }
        return poets;
    }

    public synchronized List<Poem> getAllPoems() throws SQLException {
        List<Poem> poems = new ArrayList<>();
        Statement statement = con.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * from poem");
        while (rs.next()) {
            System.out.println("poem with id :" + rs.getInt(1) + " SELECTED!");
            poems.add(new Poem(rs,con));
        }
        return poems;
    }

    public synchronized List<Category> getAllCategories() throws SQLException {
        List<Category> categories = new ArrayList<>();
        Statement statement = con.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * FROM cat");
        while (rs.next()) {
            Category category = new Category(rs, con);
            System.out.println("category with id :" + rs.getInt(1) + " SELECTED, it has " + category.getSubCats().size() + " Sub Categories!");
            categories.add(category);
        }
        return categories;
    }

}
