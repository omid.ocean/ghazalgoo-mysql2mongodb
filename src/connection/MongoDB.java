package connection;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Indexes;
import models.Category;
import models.Poem;
import models.Poet;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

public class MongoDB {
    private MongoDatabase database;

    public MongoDB() {
        MongoClient mongoClient = new MongoClient("localhost", 27017);
//        MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://<username>:<password>@ds119024.mlab.com:19024/ghazalgoo"));
        database = mongoClient.getDatabase("ghazalgoo");
    }

    public void writeCategoriesToMongoDB(List<Category> categories) {
        System.out.println("**** WRITING CATEGORIES TO MONGODB ****");
        MongoCollection<Document> collection = this.database.getCollection("categories");
        List<Document> categoryDocuments = new ArrayList<>();
        for (Category category : categories) {
            Document document = new Document();
            document.append("_id", category.getObjectId())
                    .append("title", category.getTitle())
                    .append("description", "")
                    .append("isParent", category.getParentID() == 0)
                    .append("poet", category.getPoetObjID());
            List<ObjectId> subCatIDs = new ArrayList<>();
            for (Category.SubCat subCat : category.getSubCats()) {
                subCatIDs.add(subCat.getObjectId());
            }
            List<ObjectId> subPoemIDs = new ArrayList<>();
            for (Category.SubPoem subPoem : category.getSubPoems()) {
                subPoemIDs.add(subPoem.getObjectId());
            }

            document.append("subCategories", subCatIDs)
                    .append("poems", subPoemIDs);

            categoryDocuments.add(document);
        }
        collection.insertMany(categoryDocuments);
        System.out.println("**** DONE WRITING CATEGORIES ****");
    }

    public void writePoemsToMongoDB(List<Poem> poems) {
        System.out.println("**** WRITING POEMS TO MONGODB ****");
        MongoCollection<Document> collection = this.database.getCollection("poems");
        collection.createIndex(Indexes.text("poemName"));
        List<Document> poemDocuments = new ArrayList<>();
        for (Poem poem : poems) {
            Document document = new Document();
            document.append("_id", poem.getObjectId())
                    .append("poemName", poem.getTitle())
                    .append("poemDesc", "")
                    .append("likes", new ArrayList<>())
                    .append("parentCategory", poem.getCatObjID())
                    .append("comments", new ArrayList<>())
                    .append("poet", poem.getPoetID());
            List<Document> subVerses = new ArrayList<>();
            for (Poem.Verse subVerse : poem.getVerses()) {
                Document subVerseDoc = new Document();
                subVerseDoc.append("_id", subVerse.getObjectId())
                        .append("text", subVerse.getText())
                        .append("description", "")
                        .append("order", subVerse.getvOrder())
                        .append("isFirst", subVerse.isFirst());
                subVerses.add(subVerseDoc);
            }
            if (poem.getTitleFull() != null) {
                document.append("poemNameFull", poem.getTitleFull());
            }
            document.append("verses", subVerses);
            poemDocuments.add(document);
        }
        collection.insertMany(poemDocuments);
        System.out.println("**** DONE WRITING POEMS ****");
    }

    public void writePoetsToMongoDB(List<Poet> poets) {
        System.out.println("**** WRITING POETS TO MONGODB ****");
        MongoCollection<Document> collection = this.database.getCollection("poets");
        List<Document> poetDocuments = new ArrayList<>();
        for (Poet poet : poets) {
            Document document = new Document();
            document.append("_id", poet.getObjectId())
                    .append("name", poet.getName())
                    .append("description", poet.getDesc())
                    .append("likes", new ArrayList<>())
                    .append("image", null)
                    .append("category", poet.getCatObjID());
            poetDocuments.add(document);
        }
        collection.insertMany(poetDocuments);
        System.out.println("**** DONE WRITING POETS ****");
    }

}
