import connection.MongoDB;
import connection.Mysql;

import java.sql.SQLException;

public class Main {

    public static void main(String[] args) {
        Mysql mysql = new Mysql(false);
        MongoDB mongoDB = new MongoDB();
        try {
//            mongoDB.writeCategoriesToMongoDB(mysql.getAllCategories());
            mongoDB.writePoemsToMongoDB(mysql.getAllPoems());
//            mongoDB.writePoetsToMongoDB(mysql.getAllPoets());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
